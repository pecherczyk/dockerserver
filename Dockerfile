FROM ubuntu:16.04

# Volume root directory for source code

VOLUME /app

#install apache2

RUN apt-get update && apt-get install apache2 -y

COPY config/apache2.conf /etc/apache2/
COPY config/vhost.conf /etc/apache2/sites-available
COPY config/dir.conf /etc/apache2/mods-available

RUN apt-get -y install php7.0 libapache2-mod-php7.0
RUN apt-get -y install php7.0-mysql php7.0-curl php7.0-gd php7.0-intl php-pear php-imagick php7.0-imap php7.0-mcrypt php-memcache  php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-mbstring php-gettext
RUN apt-get -y install php7.0-opcache php-apcu

COPY config/php.ini /etc/php/7.0/apache2/

RUN a2enmod rewrite
RUN a2ensite vhost.conf
RUN a2dissite 000-default.conf
RUN service apache2 restart
CMD apachectl -D FOREGROUND

EXPOSE 80
